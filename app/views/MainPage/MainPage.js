import helloWorldView from './MainPage.html';

export default ngModule => {
  ngModule.directive('mainPage', helloWorldFn);
  function helloWorldFn() {
    return {
      restrict: 'E',
      scope: {},
      template: helloWorldView,
      controllerAs: 'vm',
      controller: function () {
        const vm = this;
        
      }
    }
  }
}