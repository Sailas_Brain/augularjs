export default ngModule => {
    ngModule.controller("mainCtrl", ($scope) => {
        $scope.name = "";
        $scope.description = "";
        $scope.array = [];
        $scope.bool = false;
        $scope.textDesc = "";
        $scope.clear;
        $scope.storege = null;
        $scope.addInStorage = (name = null, description = null) => {
            $scope.storege = JSON.parse(window.localStorage.getItem("storage"));
            if(name != null && description != null) {
                if($scope.storege) {
                    const storName = $scope.storege.find(elem => {
                        return (elem.name == name && elem.name != undefined) ? elem : {};
                    });
                    $scope.bool = false;
                    if(storName.name == name)
                    {
                        $scope.bool = true;
                    }
                }
                if($scope.bool == false) {
                    $scope.array.push({name: name, description: description});
                    window.localStorage.setItem("storage", JSON.stringify($scope.array));
                    $scope.name = "";
                    $scope.description = "";
                }
            }
            else if($scope.storege) {
                $scope.array.push(...$scope.storege);
            }
        }
        $scope.addInStorage();
        $scope.validation = (event) => {
            const inputEl = document.querySelectorAll(".top-section__form input");
            for (let i = 0; i < inputEl.length; i++) {
                const element = inputEl[i];
                if(element.value == "") {
                    element.nextElementSibling.style.display = "block";
                }
                else {
                    element.nextElementSibling.style.display = "none";
                }
            }
        }
        $scope.addItem = (event, name, description) => {
            $scope.validation(event);
            if(name != "" && description != "") {
                $scope.addInStorage(name, description);
            }
        }
        $scope.viewDescription = (event) => {
            const target = event.target;
            const textCont = target.textContent.replace(/\s+/g, ' ').trim();
            const arrDisc = $scope.array.find(elem => elem.name == textCont);
            $scope.textDesc = arrDisc.description;
        }
        $scope.removeItem = (event, index) => {
            $scope.array.splice(index, 1);
            window.localStorage.setItem("storage", JSON.stringify($scope.array));
        }
        $scope.editItem = (event, index) => {
            const parenTarget = document.getElementById("list-"+index);
            const textCont = parenTarget.textContent.replace(/\s+/g, ' ').trim();
            if($scope.name == "" && $scope.description == "") {
                const arrName = $scope.array.find(elem => elem.name == textCont);
                $scope.array.splice(index, 1);
                $scope.name = arrName.name;
                $scope.description = arrName.description;
                window.localStorage.setItem("storage", JSON.stringify($scope.array));
            }
        }
    });
}