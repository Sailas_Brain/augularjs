import "./css/main.css";
import "./scss/main.scss";

const angular = require('angular');
const ngModule = angular.module('app', []);

import controllers from './controllers';
import views from './views';
// import models from './models';
controllers(ngModule);
views(ngModule);
// models(ngModule);

