module.exports = {
  context: __dirname + '/app',
  entry: __dirname + '/app/index.js',
  output: {
    path: __dirname + '/app',
    filename: './bundle.js'
  },
  module: {
    rules: [
      {test: /\.js$/, loader: 'babel-loader', exclude: "/node_modules/"},
      {test: /\.html$/, loader: 'raw-loader'},
      {test: /\.css$/, loader: 'style-loader!css-loader'},
      {test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader'}
    ]
  }
};